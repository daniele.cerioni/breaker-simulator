package it.ssg.training.intsim;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.eclipse.kura.configuration.ConfigurableComponent;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import it.bway.up.iecdispatcher.translator.DataToTranslate;
import it.bway.up.iecdispatcher.translator.ReceivedToTranslate;
import it.ssg.skyseam.up.virtual.ValueEnum;

public class IntSim implements ConfigurableComponent {

	private static final Logger logger = LoggerFactory.getLogger(IntSim.class);

	// definisco logger
	private static final String APP_ID = "it.ssg.training.intsim.IntSim";
	private Map<String, Object> properties;

	// definisco lo scheduler
	private ScheduledExecutorService exec;
	private ScheduledFuture handle;

	// definisco paratemtri mqtt
	private MqttClient mqCli;
	private String queueName = "breaker";
	private static final String mqAddress = "127.0.0.1";
	private static final Integer mqPort = 1883;

	String breaker;
	Boolean isEnable; // variabile switch del tool

	// mappa per le propriet� da aggiungere al json
	private Map<String, Object> propertiesToSend = new HashMap<String, Object>();

	protected void activate(ComponentContext ctx, Map<String, Object> properties) {
		logger.info("Bundle has started with config: {}", APP_ID);
		// mappa properties
		this.properties = properties;

		isEnable = (Boolean) properties.get("isEnabled");
		logger.info("key: {}", isEnable);

		// inizializzo lo scheduler
		exec = Executors.newSingleThreadScheduledExecutor();

		if (isEnable.equals(Boolean.TRUE)) {
			// prelevo l'intervallo
			int intervallo = (int) properties.get("intervallo.odm");
			// prelevo il numero dell'interruttore
			int numeroBreaker = (int) properties.get("numero.odm");

			// stringa dell' organo di manovra
			breaker = "breaker" + numeroBreaker;
			logger.info("Odm: {}", breaker);
		

		try {
			// sezione odm
			Integer breakerStatus = (Integer) properties.get("breaker.status");
			Boolean[] brs = new Boolean[2];
			if (breakerStatus.equals(0)) {
				brs[0] = Boolean.FALSE;
				brs[1] = Boolean.FALSE;
			} else if (breakerStatus.equals(1)) {
				brs[0] = Boolean.TRUE;
				brs[1] = Boolean.FALSE;
			} else if (breakerStatus.equals(2)) {
				brs[0] = Boolean.FALSE;
				brs[1] = Boolean.TRUE;
			} else {
				logger.error("invalid value " + brs);
			}
			propertiesToSend.put(ValueEnum.breakerStatus.toString(), brs);
			logger.info("caricate informazioni sensori: {}", propertiesToSend);

			MemoryPersistence persistence = new MemoryPersistence();
			String clientId = MqttClient.generateClientId();
			mqCli = new MqttClient("tcp://" + mqAddress + ":" + mqPort, clientId, persistence);
			logger.info("preparato mqtt");

			logger.info("avvio classe simulationjob");
			SimulationJob sj = new SimulationJob();
			handle = exec.scheduleAtFixedRate(sj, 5, intervallo, TimeUnit.SECONDS);

		} catch (Exception e) {
			logger.error("Error in initialization: {}", e.getMessage(), e);
		}

	}else{
		logger.info("SPENTO");
	}

	}


	protected void deactivate(ComponentContext ctx, Map<String, Object> properties) {
		if (isEnable.equals(Boolean.TRUE)) {
			try {
				if (handle != null)
					handle.cancel(true);
			} catch (Exception e) {
				logger.error("Error killing handle: {}", e.getMessage(), e);
			}
			try {
				exec.shutdownNow();
			} catch (Exception e) {
				logger.error("Error shutting down executor: {}", e.getMessage(), e);
			}

		}
		logger.info("Bundle {} has stopped!!", APP_ID);
	}

	public void update(ComponentContext ctx, Map<String, Object> properties) {
		deactivate(ctx, properties);
		activate(ctx, properties);
	}

	private class SimulationJob implements Runnable {

		Gson gson = new Gson();

		@Override
		public void run() {
			try {
				logger.info("entro nella routine Simulation Job");
				logger.info("dispositivo: {}", breaker);
				ReceivedToTranslate<DataToTranslate> parameter = new ReceivedToTranslate<DataToTranslate>();
				parameter.setDevice(breaker);
				parameter.setTmsUpdate(System.currentTimeMillis());
				logger.info("Prelevati parametri base");
				Map<String, DataToTranslate> factor = new HashMap<String, DataToTranslate>();
				logger.info("prelevando parametri interruttori");

				// se lo stato di breaker status � DISCONNECTED, non mandare nulla
				if ((int) properties.get("breaker.status") == 0) {
					factor.put(ValueEnum.breakerStatus.toString(), new DataToTranslate("Disconnected", null, null));
				} else {
					for (Entry<String, Object> en : propertiesToSend.entrySet()) {
						DataToTranslate dt = new DataToTranslate(en.getValue(), null, null);
						factor.put(en.getKey(), dt);
					}
					// DA CONFIGURAZIONE: Invia attiva e reattiva true/false; 1 campo att e un campo
					// reatt
					/*
					 * factor.put(ValueEnum.reactivePowerP1.toString(), new DataToTranslate(0, null,
					 * Boolean.FALSE)); factor.put(ValueEnum.reactivePowerP2.toString(), new
					 * DataToTranslate(0, null, Boolean.FALSE));
					 * factor.put(ValueEnum.reactivePowerP3.toString(), new DataToTranslate(0, null,
					 * Boolean.FALSE));
					 * 
					 * factor.put(ValueEnum.factorP3.toString(), new DataToTranslate(0, null,
					 * Boolean.FALSE)); factor.put(ValueEnum.factorP2.toString(), new
					 * DataToTranslate(0, null, Boolean.FALSE));
					 * factor.put(ValueEnum.factorP1.toString(), new DataToTranslate(0, null,
					 * Boolean.FALSE)); factor.put(ValueEnum.harmonics1.toString(), new
					 * DataToTranslate(0, null, Boolean.FALSE));
					 * factor.put(ValueEnum.harmonics2.toString(), new DataToTranslate(0, null,
					 * Boolean.FALSE)); factor.put(ValueEnum.harmonics3.toString(), new
					 * DataToTranslate(0, null, Boolean.FALSE));
					 */

					// invia lo stesso valore su tutti e tre i campi
					if (properties.get("send.voltage") == Boolean.TRUE) {
						Integer vv = (Integer) properties.get("value.voltage");
						factor.put(ValueEnum.voltageP1.toString(), new DataToTranslate(vv, null, null));
						factor.put(ValueEnum.voltageP2.toString(), new DataToTranslate(vv, null, null));
						factor.put(ValueEnum.voltageP3.toString(), new DataToTranslate(vv, null, null));
					} else {

					}

					// invia lo stesso valore su tutti e tre i campi
					if (properties.get("send.current") == Boolean.TRUE) {
						Integer cv = (Integer) properties.get("value.current");
						factor.put(ValueEnum.currentP1.toString(), new DataToTranslate(cv, null, null));
						factor.put(ValueEnum.currentP2.toString(), new DataToTranslate(cv, null, null));
						factor.put(ValueEnum.currentP3.toString(), new DataToTranslate(cv, null, null));
					} else {

					}

					// invia lo stesso valore su tutti e tre i campi
					if (properties.get("send.activepower") == Boolean.TRUE) {
						Integer apv = (Integer) properties.get("value.activepower");
						factor.put(ValueEnum.activePowerP1.toString(), new DataToTranslate(apv, null, null));
						factor.put(ValueEnum.activePowerP2.toString(), new DataToTranslate(apv, null, null));
						factor.put(ValueEnum.activePowerP3.toString(), new DataToTranslate(apv, null, null));
					} else {

					}

					// invia lo stesso valore su tutti e tre i campi
					if (properties.get("send.reactivepower") == Boolean.TRUE) {
						Integer rpv = (Integer) properties.get("value.reactivepower");
						factor.put(ValueEnum.reactivePowerP1.toString(), new DataToTranslate(rpv, null, null));
						factor.put(ValueEnum.reactivePowerP2.toString(), new DataToTranslate(rpv, null, null));
						factor.put(ValueEnum.reactivePowerP3.toString(), new DataToTranslate(rpv, null, null));
					} else {

					}
					// DA CONFIGURAZIONE: Invia correnti e tensioni true/false; 1 campo corrente e
					// un campo tensione

					// factor.put(ValueEnum.currentPN.toString(), new DataToTranslate(0, null,
					// Boolean.FALSE));
					// factor.put(ValueEnum.frequency.toString(), new DataToTranslate(0, null,
					// Boolean.FALSE));

				}
				parameter.setDatas(factor);

				String strSend = gson.toJson(parameter);

				logger.info("json generato: {}", strSend);

				MqttMessage msg = new MqttMessage(strSend.getBytes());

				mqCli.connect();

				logger.info("Mqtt avviato");

				mqCli.publish(queueName, msg);

				mqCli.disconnect();

			} catch (Exception e) {
				if (e instanceof InterruptedException) {
					logger.info("woke up");
					return;
				}
				logger.error("Error while sending simulated sensor data: {}", e.getMessage(), e);
			}
		}
	}
}
